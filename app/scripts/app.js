'use strict';

/**
 * @ngdoc overview
 * @name sceeApp
 * @description
 * # sceeApp
 *
 * Main module of the application.
 */

var translations = {
  'en': {
    main_title: "Swing into the Big Apple",
    section_title: "Fancy a break for two in New York City?",
    section_para1: "Complete our quiz in the quickest time possible and you could be in with a chance of winning exactly that. To celebrate the release of <i>The Amazing Spider-Man 2</i> at cinemas April 16, we’ve teamed up with Sony Pictures to put together a prize so big, even super-villain Electro would have trouble making off with it.",
    section_para2: "It’s great to be Spider-Man (Andrew Garfield). For Peter Parker, there’s no feeling quite like swinging between skyscrapers, embracing being the hero, and spending time with Gwen (Emma Stone). But being Spider-Man comes at a price: only Spider-Man can protect his fellow New Yorkers from the formidable villains that threaten the city. With the emergence of Electro (Jamie Foxx), Peter must confront a foe far more powerful than he. And as his old friend, Harry Osborn (Dane DeHaan), returns, Peter comes to realize that all of his enemies have one thing in common: Oscorp."
  },
  'cz': {
    main_title: "Zhoupněte se na Velkém jablku",
    section_title: "Líbilo by se ti strávit ve dvou pár dnů v New Yorku?",
    section_para1: "Vyplň co nejrychleji náš kvíz a máš šanci vyhrát. V rámci oslavy uvedení filmu <i>The Amazing Spider-Man 2</i> v kinech od 24. dubna jsme spolu se společností Sony Pictures dali dohromady cenu tak obrovskou, že by měl potíže ji odnést dokonce i superpadouch Electro.",
    section_para2: "Být Spider-Manem (Andrew Garfield) je skvělé. Peter Parker nezná úžasnější pocit než se prohánět mezi mrakodrapy, být skutečným hrdinou a trávit čas s Gwen (Emma Stone). Být Spider-Manem ale má také svou cenu: jedině Spider-Man je schopen ochránit ostatní Newyorčany před pozoruhodnými padouchy, kteří město ohrožují. Když se na scéně objevuje Electro (Jamie Foxx), je Peter nucen čelit nepříteli, který je mnohem silnější než on sám. A když se vrací i jeho starý přítel Harry Osborn (Dane DeHaan), uvědomuje si Peter, že všechny jeho nepřátele něco spojuje: společnost Oscorp."
  },
  'de': {
    main_title: "Schwing dich nach New York",
    section_title: "Wie wär's mit einer Reise für zwei nach New York?",
    section_para1: "Beantworte unser Quiz so schnell wie möglich und schon könntest du diesen Wahnsinnspreis gewinnen. <i>The Amazing Spider-Man 2</i> startet am 17. April in unseren Kinos. Aus diesem Anlass haben wir gemeinsam mit Sony Pictures ein Gewinnspiel ins Leben gerufen, bei dem sogar der Bösewicht Electro kurz das Bösesein bleiben lassen und mitmachen würde.",
    section_para2: "Als Spider-Man (Andrew Garfield) ist das Leben ein einziges Abenteuer. Für Peter Parker gibt es nichts Besseres, als von Wolkenkratzer zu Wolkenkratzer zu schwingen, das Heldendasein zu genießen und den Rest der Zeit mit Freundin Gwen (Emma Stone) zu verbringen. Aber die Kräfte von Spider-Man haben auch ihren Preis: Nur Spider-Man kann seine Mitbürger vor den mächtigen Bösewichten beschützen, die die Stadt regelmäßig bedrohen. Als Electro (Jamie Foxx) plötzlich auf der Bildfläche erscheint, muss Peter sich einem Gegner stellen, der viel mächtiger ist als er. Und als dann noch sein alter Freund Harry Osborn (Dane DeHaan) zurückkehrt, entdeckt Peter, dass seine Feinde alle eins gemeinsam haben: Oscorp."
  },
  'ru': {
    main_title: "Слетайте в «Большое яблоко»",
    section_title: "Хотите выиграть незабываемую поездку на двоих в Нью-Йорк?",
    section_para1: "Правильно ответьте на вопросы нашего конкурса, затратив как можно меньше времени, – и этот замечательный приз может стать вашим! В честь выхода на экраны фильма <i>«Новый Человек-паук: Высокое напряжение»</i> (российская премьера состоится 24 апреля), мы вместе с компанией Sony Pictures предлагаем вам настолько внушительный приз, что даже суперзлодею Электро будет нелегко его похитить.",
    section_para2: "Хорошо быть Человеком-пауком! Питер Паркер (Эндрю Гарфилд) обожает быть героем – летать на прочной паутине над городскими улицами и встречаться с красоткой Гвен (Эмма Стоун). Но на Человеке-пауке лежит и большая ответственность: лишь он способен защитить жителей Нью-Йорка от безжалостных и коварных злодеев. В лице Электро (Джейми Фокс) Питер встречает врага значительно сильнее себя. А с возвращением его старого друга Гарри Осборна (Дейн ДеХаан), Питер понимает, что его врагов связывает нечто общее: Oscorp."

  }
  
};

var app = angular.module('sceeApp', [
    'ngAnimate',
    'ui.router',
    'ngTouch',
    'ngSanitize',
    'pascalprecht.translate',
    'sceeApp.controllers'
    
  ]);
  app.config(['$stateProvider', '$urlRouterProvider', '$translateProvider', 
    function ($stateProvider, $urlRouterProvider, $translateProvider) {

      $translateProvider.translations('en', translations.en);
      $translateProvider.translations('cz', translations.cz);
      $translateProvider.translations('de', translations.de);
      $translateProvider.translations('ru', translations.ru);
      $translateProvider.preferredLanguage('en');


      $translateProvider.useSanitizeValueStrategy('escaped');

      $stateProvider

      .state('Main', {
        url: '/',
        controller: 'MainCtrl',
        templateUrl: 'views/main.html'
      });

      $urlRouterProvider.otherwise('/');

      

  }]);
