'use strict';

/**
 * @ngdoc function
 * @name sceeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sceeApp
 */
angular.module('sceeApp.controllers', [])
  .controller('MainCtrl', ['$scope', '$translate',
  	function ($scope, $translate) {

  		$scope.langGroup = [
  			{title:'en', flag:'gb'}, 
  			{title:'cz', flag:'cz'}, 
  			{title:'de', flag:'de'}, 
  			{title:'ru', flag:'ru'}
  		];
  		$scope.defLang = $scope.langGroup[0].title;

    	$scope.changeLang = function (langKey) {
    		$translate.use(langKey);
    	};
  }]);
